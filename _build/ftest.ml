open Graph
open Printf

(*Fonction somme d'éléments d'une liste d'arc sortant (en string)*)
let rec sum l acu id = match l with
    | [] -> acu
    | (x, arc) :: rest -> if(x==id) then sum rest acu id else sum rest (acu + int_of_string arc) id


(* Fonction test *)
let rec test = function
| [] -> ()
| e::l -> print_string e ; test l

let () =

  if Array.length Sys.argv <> 5 then
    begin
      Printf.printf "\nUsage: %s infile source sink outfile\n\n%!" Sys.argv.(0) ;
      exit 0
    end ;

  let infile = Sys.argv.(1)
  and outfile = Sys.argv.(4)
  
  (* These command-line arguments are not used for the moment. *)
  and _source = Sys.argv.(2)
  and _sink = Sys.argv.(3)
  in

  (* Open file *)
  let graph = Gfile.from_file infile in

  

  (* Application of Ford Fulkerson on the graph that has been read. *)
  let gra= Ford.ford graph _source _sink in
  (* Display of the maximum flow value found by the algorithm. *)
  let outarcs= Graph.out_arcs gra _sink in
  let ()= printf "Maximum flow of the graph : %d\n" (sum outarcs 0 _sink) in
  (* Rewrite the graph that has been read. *)
  let () = Gfile.export outfile gra in 

  ()
