open Graph

(*Petite fonction pour transformer une option en string*)
let string_of_option opt = match opt with
	| Some x -> x
	| None -> failwith "int_of_option: does not work."


(* --------------------------------------------------------------------------------------- *)


(* On trouve un chemin qui part de la source et va vers le puit *)
let findingpath graph source sink =
	(*On check si la source et le puit existent*)
	if (node_exists graph source) then 
		if (node_exists graph sink) then 
			(*Début du code ici*)
			let rec fp outarcs id acu =  match outarcs with 
				| [] -> [] (*IMPORTANT: Ici, on a choisit que findingpath renvoie [] quand il ne trouve aucun chemin*)
				| (destination, arc) :: rest -> if ( arc = "0" || List.mem destination acu) (*Si l'arc a pour valeur 0 ou que le noeud existe déjà dans le chemin.*)
													then fp rest id acu (*Alors on passe au noeud suivant*)
												else if (destination=sink && arc != "0")  (*Ou si la destination est le puit et que l'arc n'est pas nul.*)
													then List.rev (sink :: acu) (*Alors on renvoie le chemin trouvé.*)
												else if (fp (Graph.out_arcs graph destination) destination (destination :: acu) = []) (*Ou si l'itération suivante ne trouve pas de chemin.*)
													then fp rest id acu  (*Alors on passe au noeud suivant.*)
												else fp (Graph.out_arcs graph destination) destination (destination :: acu) (*Sinon on avance.*)
			in fp (Graph.out_arcs graph source) source [source]
			(*Fin du code ici*)
		else failwith "Sink doesn't exist."
	else failwith "Source doesn't exist."
		


(* --------------------------------------------------------------------------------------- *)


		
(* On recherche ici le flow du chemin en cours*)
let maxflow graph idlist =
	let rec max idlist flow = match idlist with
		| [] -> flow
		| x :: [] -> flow
		| x :: rest -> if (int_of_string (string_of_option (find_arc graph x (List.hd rest))) < flow) then max rest (int_of_string (string_of_option (find_arc graph x (List.hd rest)))) else max rest flow
	in max idlist (int_of_string (string_of_option (find_arc graph (List.hd idlist) (List.nth idlist 1))))



(* --------------------------------------------------------------------------------------- *)



(*Augmenting path, prend une list de node (le chemin trouvé par findingpath) en entrée, et renvoie un nouveau graphe avec les arcs mit à jour*)
let augmentingpath graph idlist maxflow =
	let rec ap gr idl = match idl with 
		| [] -> gr
		| x :: [] -> gr
		| x :: rest -> if (find_arc gr (List.hd rest) x = None) (* Si il n'existe pas déjà un double-arc *)
					(* Alors on créer un nouvel arc dans le graphe de flow qui va dans l'autre sens que l'ancien ET on retire sa valeur à l'ancien arc.*)
					then ap (add_arc (*graph*)(add_arc gr x (List.hd rest) (string_of_int (int_of_string (string_of_option (find_arc graph x (List.hd rest)))-maxflow)))(*/grapĥ*) (List.hd rest) x (string_of_int maxflow)) rest
					(* Sinon, si il existait déjà un arc dans l'autre sens, alors on rajoute le flow à sa valeur déjà présente ET on retire sa valeur à l'arc opposé.*) 
					else ap (add_arc (*graph*)(add_arc gr x (List.hd rest) (string_of_int (int_of_string (string_of_option (find_arc graph x (List.hd rest)))-maxflow)))(*/graphe*) (List.hd rest) x (string_of_int (int_of_string (string_of_option (find_arc graph (List.hd rest) x))+maxflow))) rest 
	in ap graph idlist



(* --------------------------------------------------------------------------------------- *)



(* Enfin on boucle jusqu'à ne plus trouver de chemin (quand findingpath renvoie []), l'algorithme de Ford-Fulkerson est donc terminé.*)
let ford graph source sink = 
	let rec f gr idlist = 
		if (idlist == []) then gr 
		else f (augmentingpath gr idlist (maxflow gr idlist)) (findingpath (augmentingpath gr idlist (maxflow gr idlist)) source sink)
	in f graph (findingpath graph source sink)
