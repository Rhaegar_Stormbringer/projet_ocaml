open Graph

(* Initialiser la source et le puit*)
(*val sourcesink : string graph -> string -> string -> string graph*)

(* Initialiser tout les arcs du graphe *)
(*val initializing : string graph -> (int * int) graph*)

(* Trouver un chemin dans le graphe *)
val findingpath : string graph -> id -> id -> id list

(*Trouver le flow maximum *)
val maxflow : string graph -> id list -> int

(*Augmenter le chemin par flow*)
val augmentingpath : string graph -> id list -> int -> string graph

(*Algorithme de Ford-Fulkerson*)
val ford : string graph -> id -> id -> string graph
